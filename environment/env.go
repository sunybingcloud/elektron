package environment

// Environment Variables and that are used.
// These environment variables need to be set before launching elektron.

// Password for username="rapl" on the nodes in the cluster.
var RaplPassword = "RAPL_PSSWD"

// Location of the script that sets the powercap value for a host.
var RaplThrottleScriptLocation = "RAPL_PKG_THROTTLE_SCRIPT_LOCATION"
