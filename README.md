Elektron: A Pluggable Mesos framework with power-aware capabilities
===================================================================

Elektron is a Mesos framework that behaves as a playground for developers to experiment with different scheduling policies to launch ad-hoc jobs.
Elektron is designed as a lightweight, configurable framework, which can be used in conjunction with built-in power-capping policies to reduce the peak power and/or energy usage of co-scheduled tasks.

#Features
* [Pluggable Scheduling Policies](docs/SchedulingPolicies.md)
* [Pluggable Power-Capping strategies](docs/PowerCappingStrategies.md)
* Cluster resource monitoring
